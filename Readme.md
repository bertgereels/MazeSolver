# MazeSolver

## Information
MazeSolver assignment for my algorithms course.

## What it has to do:
Implement a maze solver which finds the shortest path in a perfect maze, this is a maze with a single entry, exit and solution.<br/>
The maze consists of wall elements and free space represented by 'X' and ' ' respectively.<br/>
The entrance is a free space element at the left and any free space element at the right counts as an exit.<br/>
A maze generator which fulfills these criteria can be found at: https://github.com/pcordemans/MazeGenerator. <br/>
The input maze must be read from file.<br/>
Describe your algorithm in a README.MD file which algorithm you have chosen, how it works and why you have chosen it.<br/>

## Algorithm explanation:
The main solving algorithm is 'solve()', this method can be found in the 'MazeSolver' class.
I used a recursive method to solve the maze.
Before I call the 'solve()' method, I start of by searching for the entry point (start) and the end point of the maze.
When the start and end are located, I change their value (character) in the array that stores the maze.
Now the 'solve()' method gets executed.
First of all the given coordinates (start) are checked for being a wall, or a piece of the maze already visited.
If the coordinates do not correspond to a wallpiece, the method will recall itself in all directions.
If the path does not lead to the end, the function backtracks, erasing the path from the maze.
When the coordinates correspond to the end, the method returns true.
The path is saved in the maze, and gets printed.

## Sources
https://stackoverflow.com/questions/44615987/how-to-compare-a-string-with-a-char?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
http://www.java2s.com/Code/Java/File-Input-Output/Readfilecharacterbycharacter.htm
https://en.wikipedia.org/wiki/Maze_solving_algorithm
https://stackoverflow.com/questions/33092897/using-an-array-of-strings-to-create-a-2d-map-array-in-java/33094654#33094654
https://codereview.stackexchange.com/questions/187001/simple-maze-solving-algorithm?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
