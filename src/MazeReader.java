import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MazeReader {
	
	public MazeReader() {
		
	}
	
	public String readMazeFromFile(String filePath) {
		StringBuffer stringBuffer = new StringBuffer();
		try {
			File file = new File(filePath);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String st = stringBuffer + "";
		st = st.replace("\n", "").replace("\r", "");
		return st;
	}

}
