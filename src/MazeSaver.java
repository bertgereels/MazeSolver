import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MazeSaver {

	public MazeSaver() {
		
	}
	
	public void saveMazeAsTxt(Maze mazeToSave) {
		List<String> lines = mazeToSave.toListOfStrings();
		String filename = "maze.txt";
		
		Path file = Paths.get(filename);
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
