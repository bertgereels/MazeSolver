import java.util.Scanner;

public class MazeSolver {
	private Scanner reader = new Scanner(System.in); 
	private int mazeSize;
	private int xStart, yStart;
	private int xEnd, yEnd;
	
	public MazeSolver(){
		xStart = yStart = 0; 
		xEnd =  yEnd = 0;
	}
	
	public static void main(String [ ] args) {
		MazeSolver mySolver = new MazeSolver();
		mySolver.promptUserForMazeSize();
		Maze myMaze = new Maze(mySolver.getMazeSize());
		myMaze.print();
		MazeSaver myMazeSaver = new MazeSaver();
		myMazeSaver.saveMazeAsTxt(myMaze);
		MazeReader myMazeReader = new MazeReader();
		String mazeToConvert = myMazeReader.readMazeFromFile("maze.txt");
		StringBuilder sb = new StringBuilder(mazeToConvert);
		Char2D mazeAsCoords = new Char2D(sb, mySolver.getMazeSize());
		
		mySolver.getStartPos(mazeAsCoords);
		mySolver.getEndPos(mazeAsCoords);
		mySolver.addMarkers(mazeAsCoords);

		if(mySolver.solve(mySolver.getXStart(), mySolver.getYStart(), mazeAsCoords)) {
			System.out.println("The solved maze:");
		    mazeAsCoords.printMaze();
		}
		else {
			System.out.println("The maze could not be solved");
		}   
	}
	
	private boolean solve(int startX, int startY, Char2D mazeToSearch) {
		//if wall detected, true false
		if (mazeToSearch.getCh(startX, startY) == 'X') {
			return false;
	    }

		//if endpoint is detected, return true
		//change endpoint marker to match path character
		if (mazeToSearch.getCh(startX, startY) == 'E') {
			mazeToSearch.setCh('*', startX, startY);
	        return true;
	    }

		//if search path is detected, return false;
	    if (mazeToSearch.getCh(startX, startY) == '*') {
	        return false;
	    }

	    //build path
	    mazeToSearch.setCh('*', startX, startY);
	        
	    //Recall method to go one to the right
	    if ((solve(startX + 1, startY, mazeToSearch)) == true) {
	    	System.out.println("Right");
	    	return true;
	    }
	    //Recall method to go one up
	    if ((solve(startX, startY - 1, mazeToSearch)) == true) {
	    	System.out.println("Up");
	    	return true;
	    }
	    //Recall method to go one down
	    if ((solve(startX , startY + 1, mazeToSearch)) == true) {
	    	System.out.println("Down");
	    	return true;
	    }
	    //Recall method to go one to the left
	    if ((solve(startX - 1 , startY, mazeToSearch)) == true) {
	    	System.out.println("Left");
	    	return true;
	    }       

	    //If the path does not lead to a solution, backtrack.
	    //Erase path from maze.
	    mazeToSearch.setCh(' ', startX, startY);
	    
	    //Uncomment this to see all the attempts at solving the maze.
	    mazeToSearch.printMaze();
	    return false;
	}
	
	private void addMarkers(Char2D mazeToSearch) {
		mazeToSearch.setCh('S', xStart, yStart);
		mazeToSearch.setCh('E', xEnd, yEnd);
	}

	private void getStartPos(Char2D mazeToSearch) {
		for(int i = 0; i <= (getMazeSize() * 2); i++) {
			if(compareCharToString(mazeToSearch.getCh(0, i), " ")) {
				System.out.println("Start point found at: 0," + i);
				xStart = 0;
				yStart = i;
				break;
			}
		}
	}
	
	private void getEndPos(Char2D mazeToSearch) {
		int colSize = mazeToSearch.getColCount();
		for(int i = 0; i <= (getMazeSize() * 2); i++) {
			if(compareCharToString(mazeToSearch.getCh(colSize - 1, i), " ")) {
				System.out.println("End point found at: " + colSize + "," + i + "\n");
				xEnd = colSize-1;
				yEnd = i;
				break;
			}
		}
	}
	
	private boolean compareCharToString(char c, String s) {
		 if (s != null && s.length() == 1){ 
		        return s.charAt(0) == c;
		    }
		    return false;
	}
	
	private void promptUserForMazeSize() {
		System.out.println("Enter the size of the maze you wish to generate:");
		int temp = reader.nextInt();
		setMazeSize(temp);
		reader.close();
	}
	
	public void setMazeSize(int size) {
		if(size > 0) {
			mazeSize = size;
		}else {
			System.out.println("Enter a number larger than 1!");
		}
	}
	
	public int getMazeSize() {
		return mazeSize;
	}
	
	public int getXStart() {
		return xStart;
	}
	
	public int getYStart() {
		return yStart;
	}

}
