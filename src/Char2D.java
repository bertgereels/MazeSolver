public class Char2D {
	private int rows, columns;
	private StringBuilder sb;
	private char[][] convertedMaze;
	
	public Char2D(StringBuilder rawMaze, int dim) {
		rows = columns = (dim * 2) + 1;
		sb = rawMaze;
		convertedMaze = convertMazeTo2DChar();
	}
	
	private StringBuilder getSb()
    {
        return sb;
    }

    public int getRowCount()
    {
        return rows;
    }

    public int getColCount()
    {
        return columns;
    }
    
    public int xy2idx(int x, int y)
    {
        int idx = y * getRowCount() + x;
        return idx;
    }

    public Character getCh(int x, int y)
    {
    	return getSb().charAt(xy2idx(x, y));
    }
    
    public void setCh(Character ch, int x, int y)
    {
    	getSb().setCharAt(xy2idx(x, y), ch);
    }
    
    public void printMaze() {
    	char[][] maze = convertMazeTo2DChar();
    	for(int j = 0; j < columns; j++) {
    		for(int i = 0; i < rows; i++) {
          		 System.out.print(maze[i][j]);
    		}
    		System.out.println("");
    	}
	    	
    }
    
    public char[][] getMazeAs2DChar(){
    	return convertedMaze;
    }
    
    private char[][] convertMazeTo2DChar() {
    	char[][] mazeToReturn = new char[rows][columns];
    	for(int i = 0; i < columns; i++) {
    		for(int j = 0; j < rows; j++) {
          		mazeToReturn[i][j] = getSb().charAt(xy2idx(i, j));
    		}
    	}
    	return mazeToReturn;
    }
    
}
